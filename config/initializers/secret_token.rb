# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'b3f4cd3c3de6f28913b0a36abe7025a2f77d8b998bc1776157e91ddcf021658fc5e114ef044a2bed484da0036afdec0bfa7ca49a00ffc960d419033fd5e6c7ec'
